<?php

	////////////////////////// CONNECTION AND CONFIG DATABASE SETTINGS //////////////////////////

    // connect to database.
    include_once "config/mysqli_connect.php";

	// pulls all data from ConfigSettings table.
	$sqlConfigSettingsGetData = 'SELECT * FROM ConfigSettings';

	// Converts mysqli query into an array.
	$sqlConfigSettings = mysqli_query( $conn, $sqlConfigSettingsGetData );
	$sqlConfigSettingsArray = [];

	while ( $row = mysqli_fetch_array( $sqlConfigSettings, MYSQLI_ASSOC ) ){
	   $sqlConfigSettingsArray[] = $row;
	}
	
	////////////////////////// VARIABLES USED FOR HEADER.PHP //////////////////////////
	
	// if you had any idea what you were doing you'd move this to header.php.

	// Used to identify how many rows are on the ConfigSettings Database. In v3, this is used in header.php
	$psVersion = $sqlConfigSettingsArray[0]['PsVersionNo'];
	
	if ( $psVersion == 3 ){ 	
		// pulls all data from ConfigSettings table.
		$esxiDataQuery = 'SELECT * FROM EsxiData';

		// Converts mysqli query into an array.
		$esxiDataTable = mysqli_query( $conn, $esxiDataQuery );
		$esxiDataArray = [];
		
		while ( $row2 = mysqli_fetch_array( $esxiDataTable, MYSQLI_ASSOC ) ){
			$esxiDataArray[] = $row2;
		}
	}

	////////////////////////// DRIVE INFO CHECKS //////////////////////////

	// Initialize string variable
	$hideDrivesSet = '';

	// if Hide Removed Drives is set in config settings, then set empty string to an SQL command...
	if ( $sqlConfigSettingsArray[0]['hideremoveddrives'] === 1 ){
		$hideDrivesSet = 'WHERE IsPresent = 1';
	}

	// Gets all data from the DriveInfo table.
	$allDrivesSQL = 'SELECT * FROM DriveInfo $hideDrivesSet ORDER BY DiDriveId;';
	$mysqliAssocDrives = mysqli_query( $conn, $allDrivesSQL );
    $drives = mysqli_fetch_all( $mysqliAssocDrives, MYSQLI_ASSOC );

	// foreach drive, set variables for both drive temp low and high thresholds.
	foreach ( $drives as $drive ){
		$i = $drive['DiDriveId'];
		$driveTempLow[$i] = $drive['DriveTempLow'];
		$driveTempHigh[$i] = $drive['DriveTempHigh'];
    }

	// if remove hard drive hasn't been set (is null), or it's set true (1) then tick the 'checked' box on 'hide removed drives' on settings page.
        if ( $sqlConfigSettingsArray[0]['hideremoveddrives'] == '' || $sqlConfigSettingsArray[0]['hideremoveddrives'] == '1' ){
            $sqlHideRemovedDrives = "checked='checked'";
        } else {
            $sqlHideRemovedDrives = '';
        }

	////////////////////////// SUBMIT BUTTON //////////////////////////

    // Receives data when SUBMIT pressed.
    if ( isset( $_POST['submit'] )){
        foreach ( $drives as $drive ){
            // Name of hidden input in form for drive threshold
            $driveTempThreshold = 'driveTempThresholdSlider' . $drive['DiDriveId'];

            // Take ranges of drive thresholds and update DriveInfo database.
            if ( isset( $_POST["$driveTempThreshold"] )){
				                
                // Splits Temperature Threshold string from 25 - 30 into 25; 30; thus creating intega's for database update.
                $sqlDriveThreshold = explode( " ", trim( ( $_POST["$driveTempThreshold"] ) ) );
                $sqlDriveLow  = $sqlDriveThreshold[0];
                $sqlDriveHigh = $sqlDriveThreshold[2];
                $driveID      = $drive['DiDriveId'];

                // SQL Update injection command.
                $sqlDriveTempInjection = "UPDATE `DriveInfo` SET `DriveTempHigh` = $sqlDriveHigh, 
                    `DriveTempLow` = $sqlDriveLow 
                    WHERE DiDriveId = $driveID";

                // Updates SQL database. If error then write out.
                if ( !mysqli_query( $conn, $sqlDriveTempInjection )){
                    echo "Unable to update DriveInfo table with DriveTempHigh/DriveTempLow parameters " . mysqli_error( $conn );
                }
            }
        }

        // Updates the hdsort value on ConfigSettings table when the default value is NOT selected.
        if ( isset( $_POST['selectdropdown'] )){

            if ( !$_POST['selectdropdown'] == 0 ){
                $hdSort = $_POST['selectdropdown'];

                // SQL update query.
                $sqlDriveSortOrderInjection = "UPDATE `ConfigSettings` SET `hdsort` = $hdSort";

                // Execute query.
                if ( !mysqli_query( $conn, $sqlDriveSortOrderInjection )){
                    echo "Unable to update ConfigSettings table with Drive Order settings " . mysqli_error( $conn );
                } else {
					header( 'Location: index.php');
				}
            }  
        }

		// Alters the 'Hide Removed Drives' Checkbox display.
        if ( isset( $_POST['checkbox'] )){
            $ignoreRemovedDeadDrives = 1;
        } else {
            $ignoreRemovedDeadDrives = 0;
        }

		// SQL injection query to hide removed drives.
        $sqlRemoveDeadDrivesInjection = "UPDATE `ConfigSettings` SET `hideremoveddrives` = $ignoreRemovedDeadDrives";

        // Execute query.
        if ( !mysqli_query( $conn, $sqlRemoveDeadDrivesInjection )){
            echo "Unable to update ConfigSettings table with ignore removed drive setting. " . mysqli_error( $conn );
        } else {
            header( 'Location: index.php');
        }
    }

    // close connection
    mysqli_close( $conn );

?>

<!DOCTYPE html>
<html lang="en">

    <?php include( 'templates/header.php' ); ?>
  
    <section class="section container center">
        <br />
        <h6>Site Config Settings</h6>
       
    </section>

    <section class="section container">
        <div class="class col s2 offset-s5">
            <!-- Form for collecting and updating information used for the configuration settings -->
            <form action="settings.php" method="POST">
            <div class="input-field col s6 offset-s3">
                    <!-- Drop down box -->
                    <select id="myselect" onchange="singleSelectChangeValue()">
                        <option value="0" disabled selected>Hard Drive Sort Order</option>
                        <option value="1">Large to Small</option>
                        <option value="2">Small to Large</option>
                        <option value="3">Controller Location</option>
                        <option value="4">Raw Device Mapping Name</option>
                        <option value="5">Hard Disk No.</option>
						<option value="6">Hard Disk Type</option>
                    </select>
                </div>

				<div>&nbsp;</div>
				<div class="tooltipped" data-position="bottom-right" data-delay="500" enterDalay="2000" data-tooltip="If a drive has been removed/repaced from ESXi, remove said drive from this website and email alerts.
					<br />NOTE: Drives will stay on site and email alerts until a scheduled task has completed successfully">
                    <label>
                        <!-- Checkbox for disabling removed drives -->
                        <input type='checkbox' name='checkbox' id='checkbox' class='filled-in' <?php echo "$sqlHideRemovedDrives"; ?> >
                        <span class="black-text">Hide drives no longer present</span>
                    </label>
                </div>

                <br />



                <p>&nbsp;</p>

                <!-- Submit button -->
                <div class="center">
                    <input type="submit" name="submit" value="submit" class="btn settings z-depth-2">
                </div>

                <?php
                    // Creates hidden inputs for each drive config temp threshold.
                    
                    foreach ( $drives as $drive ){
                        $driveTempThresholdSliderInput = 'driveTempThresholdSlider' . $drive['DiDriveId'];
                        echo "<input type='hidden' name='$driveTempThresholdSliderInput' id='$driveTempThresholdSliderInput' ></input>";

                        $driveTempCountSliderInput = 'driveTempCountSlider' . $drive['DiDriveId'];
                        echo "<input type='hidden' name='$driveTempCountSliderInput' id='$driveTempCountSliderInput' ></input>";
                    }

                    // hidden label for hdsort select drop down boxes. The java script is part of scripts.php
                    echo "<input type='hidden' name='selectdropdown' id='selectdropdown' ></input>";
                                        
                ?>

            </form>
        </div>
    </section>
 
    <div class="row">
        <?php 
            foreach ( $drives as $drive ){

                //Changes ESX CanonicalName into something far more readable
                $driveName = str_replace( 't10.ATA_____', '', $drive['Canonical_Name'] );
                $driveName = str_replace( "_", " ", $driveName );

                // Hidden variables used for drive temp threshold alerts.
                $i = $drive['DiDriveId'];
                $sliderThreshDiv = "sliderThreshDiv" . $drive['DiDriveId'];
                $sliderThreshHiddenInput = 'driveTempThresholdSlider' . $drive['DiDriveId'];
				
				// Variables defined by drive type. i.e. SSD, NVMe and spinning rust will change colours and images.
				if ( $drive['SSD'] == 1 ){ 
					$thresholdLow = 0;
					$thresholdHigh = 80;

					if ( strpos( $driveName, "NVMe" )){ 
						$driveName = str_replace( 't10.NVMe', '', $drive['Canonical_Name'] );
						$driveName = str_replace( "_", " ", $driveName ); 
						$imageName = "nvme.svg";
						$imageClass = "imagesm2";
						$cardColour = "blue lighten-2";
					} else {
						$imageName = "ssd.png";
						$imageClass = "images"; 
						$cardColour = "blue lighten-3";
					}
				} else {
					$thresholdLow = 20;
					$thresholdHigh = 60;
					$imageName = "hdd.svg";
					$imageClass = "images";
					$cardColour = "blue lighten-4";
				} ?>

                <div class="col s12 m4 l3">
                    <div class="card z-depth-2 <?php echo $cardColour; ?>">

                        <!-- HDD Images -->
                        <img src="images/<?php echo $imageName; ?>" class="<?php echo $imageClass; ?>">

                        <!-- Reveal card  upper limit start -->
                        <div class="card blue-grey lighten-5">
                            <div class="card-image waves-effect waves-block waves-light">

                                <!-- Drive details as a card -->
                                <div class="card-content center">
                                    <h6><?php echo htmlspecialchars( $driveName );?></h6>
                                    <ul>
										<li><?php echo "SQL Drive ID " . htmlspecialchars( $drive['DiDriveId'] )?></li>
                                        <li><?php echo htmlspecialchars( $drive['Capacity'] ) . " GB" ?></li>
                                        <li>&nbsp;</li>
                                        <li>&nbsp;</li>

                                        <li>Temperature Threshold (min to max)</li>
                                        <li>&nbsp;</li>
                                        <li>&nbsp;</li>
                                        <div id="<?php echo $sliderThreshDiv;?>" class="noUi-target noUi-ltr noUi-horizontal"></div>

                                        <script>
                                            // creates the slider for the temp threshold.
                                            var slider = document.getElementById('<?php echo $sliderThreshDiv;?>');
                                            noUiSlider.create(slider, {
                                                start: [<?php echo $driveTempLow[$i]; ?>, 
                                                    <?php echo $driveTempHigh[$i]; ?>],
                                                connect: true,
                                                step: 1,
                                                orientation: 'horizontal',
                                                range: {
                                                    'min': <?php echo $thresholdLow; ?>,
                                                    'max': <?php echo $thresholdHigh; ?>
                                                },
                                                format: wNumb({
                                                    decimals: 0
                                                })
                                            });
                                        </script>

                                        <script>
                                                // updates the hidden input value of each drive threshold value
                                                slider.noUiSlider.on('update', function (values) {
                                                document.getElementById( '<?php echo $sliderThreshHiddenInput;?>' ).value = values.join(' - ');
                                            });
                                        </script>
										
                                        <li>&nbsp;</li>
                                        <li>&nbsp;</li>
                                    </ul>
									
                                </div>
                            </div>	
                        </div>
                    </div>
                </div>
				
            <?php } // Closes foreach drive
        ?> <!-- Closes php code -->
    </div>
	

    <!-- Apparantely can't be in header? -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

    <?php include( 'templates/footer.php'); ?>

</html>