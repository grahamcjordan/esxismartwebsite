<?php

	// connect to database.
    include_once "config/mysqli_connect.php";

	// This little puppy will combine tables. It captures date, temp, canonical_name and driveId of just drive 3, and the latest 3 results.
	// SELECT drivestats.Date, drivestats.Temperature, driveinfo.Canonical_Name, driveinfo.DiDriveId FROM drivestats INNER JOIN driveinfo ON driveinfo.DiDriveId = DriveStats.DiDriveId WHERE drivestats.DiDriveId = 3 ORDER BY Date DESC limit 3

	////////////////////////// DATABASE VERSION CHECKS AND UPGRADE //////////////////////////

	// Gets the configsettings table
    $sqlConfigSettingsCreated = mysqli_query( $conn, 'select 1 from `ConfigSettings` LIMIT 1' );

    // if ConfigSettings already exists in database (version 2 onwards )...
    if ( $sqlConfigSettingsCreated !== FALSE ){

		// pulls all data from ConfigSettings table.
        $sqlConfigSettingsGetData = 'SELECT * FROM ConfigSettings';

        // Converts mysqli query into an array.
        $sqlConfigSettings = mysqli_query( $conn, $sqlConfigSettingsGetData );
		$sqlConfigSettingsArray = [];

		while ( $row = mysqli_fetch_array( $sqlConfigSettings, MYSQLI_ASSOC ) ){
		   $sqlConfigSettingsArray[] = $row;
		}

		// Counts how many columns exist in ConfigSettings.
		$no = count( current( $sqlConfigSettingsArray ) );

		// If the column count is 3, then the version number of the database is only 2. Therefore needs upgrading.
		if ( $no === 3 ){
			$sqlUpdateToVersion3 = '
				ALTER TABLE configsettings 
				ADD COLUMN WebVersionNo INT(255) NOT NULL,
				ADD COLUMN PsVersionNo INT(255) NOT NULL;';
					
			$sqlUpdateToVersion3.= 'UPDATE configsettings SET WebVersionNo = 3;';
			$sqlUpdateToVersion3.= 'UPDATE configsettings SET PsVersionNo = 2;';
			$sqlUpdateToVersion3.= 'UPDATE configsettings SET graphplotcount = 48;';
					
			$conn->multi_query( $sqlUpdateToVersion3 );
		} else {
			// Here is where you would account for version 3 moving up to 4.
			if  ( $sqlConfigSettingsArray[0]['WebVersionNo'] === 3 ){
				// echo "update version no to 4" ;
			} else {
				//echo "you're already on 3" ;
			}
		}
    } else {
        // if the ConfigSettings table does not exist, create it. 
        $sqlConfigSettingsCreation = 'CREATE TABLE `ConfigSettings` (  
            `hdsort` INT(255) NULL DEFAULT NULL, 
            `graphplotcount` INT(255) NULL DEFAULT NULL,
            `hideremoveddrives` TINYINT(1) NULL DEFAULT NULL,
			`WebVersionNo` INT(255) NOT NULL,
			`PsVersionNo` INT(255) NOT NULL
        );';

		// sets defaults for configsettings. 
		//		Sort by Hard Type (Spinning disk, SSD, NVMe)
		//		48 plot points on the graph (if scheduled task is set to 30 minutes, then provides a day of data)
		//		Automatically removes drives taken out of NAS from website. Does not delete from database.
		//		Sets version number to 3.
		$sqlConfigSettingsCreation.= 'INSERT INTO `ConfigSettings`( `hdsort`, `graphplotcount`, `hideremoveddrives`, `WebVersionNo`, `PsVersionNo` )
            VALUES ( 6, 48, 1, 3, 2 );';

		// runs queries
		$conn->multi_query( $sqlConfigSettingsCreation );
    }

	////////////////////////// UPGRADE CHECKS //////////////////////////

	// Gets the configsettings table
    $sqlConfigSettingsCreated = mysqli_query( $conn, 'select 1 from `ConfigSettings` LIMIT 1' );
	
	if ( $sqlConfigSettingsCreated == FALSE ){
		echo "Warning. Your database is missing 'ConfigSettings' and cannot be updated. Please refresh this page or contact the developer on Xpenology<br />";
	} else {
		// pulls all data from ConfigSettings table.
        $sqlConfigSettingsGetData = 'SELECT * FROM ConfigSettings';

        // Converts mysqli query into an array.
        $sqlConfigSettings = mysqli_query( $conn, $sqlConfigSettingsGetData );
		$sqlConfigSettingsArray = [];

		while ( $row = mysqli_fetch_array( $sqlConfigSettings, MYSQLI_ASSOC ) ){
		   $sqlConfigSettingsArray[] = $row;
		}

		// Counts how many columns exist in ConfigSettings.
		$no = count( current( $sqlConfigSettingsArray ) );
		
		if ( !$no === 3 ){
			echo "Warning. 'ConfigSettings' has not updated to version 3. Please refresh this page. If you see this error again contact the developer on Xpenology<br />";
		} else { 
			// Here is where you would account for version 3 moving up to 4.
			if  ( $sqlConfigSettingsArray[0]['WebVersionNo'] === 3 ){
				echo "Warning! 'ConfigSettings' has upgraded to version 3, but data settings have not been written to the database. Please refresh this page. If you see this error again contact the developer on Xpenology<br />";
			} else {
				 // echo "you're already on 3" ;
			}
		}
	}

	////////////////////////// DRIVE INFO CHECKS //////////////////////////

	// Gets all data from the DriveInfo table.
	$allDrivesSQL = 'SELECT * FROM DriveInfo;';
	$mysqliAssocDrives = mysqli_query( $conn, $allDrivesSQL );
    $drives = mysqli_fetch_all( $mysqliAssocDrives, MYSQLI_ASSOC );
	
	////////////////////////// DRIVE THRESHOLD DEFAULTS //////////////////////////

    // Creates suggested values for drive temp alerts.
    foreach ( $drives as $drive ){
        $i = $drive['DiDriveId'];

        if ( $drive['SSD'] == '1' ){
            $driveTempLow[$i] = 10;
            $driveTempHigh[$i] = 60;
        } else {
            $driveTempLow[$i] = 25;
            $driveTempHigh[$i] = 45;
        }
    }

    // Checks if a column DriveTempHigh exists on the first drive.
    if ( isset( $drives[0]['DriveTempHigh'] )){

        foreach ( $drives as $drive ){

            // If column DriveTempHigh does exist and is set, obtain values.
            if ( !$drive['DriveTempHigh'] == '0' ){

                $i = $drive['DiDriveId'];
                $driveTempLow[$i] = $drive['DriveTempLow'];
                $driveTempHigh[$i] = $drive['DriveTempHigh'];
            }
        }
    } else {
        // If upgrading from version 1, columns do not exist, create them. 
        $sqlAlterTable = 'ALTER TABLE DriveInfo 
            ADD COLUMN DriveTempHigh INT(255) NOT NULL,
            ADD COLUMN DriveTempLow INT(255) NOT NULL,
            ADD COLUMN IsPresent TINYINT(1) NOT NULL;';
            
        if ( !mysqli_query( $conn, $sqlAlterTable )){
            echo "Unable to add columns 'DriveTempHigh' and 'DriveTempLow' to 'DriveInfo'" . mysqli_error( $conn );
        }

        $setDrivePresent = "UPDATE `DriveInfo` SET `IsPresent` = 1 WHERE 1;";

        if ( !mysqli_query( $conn, $setDrivePresent )){
            echo "Unable to set 'IsPresent' to 1 on 'DriveInfo'. If ticked, index will appear empty until a scheduled task has completed." . mysqli_error( $conn );
        }
    }
	
	header( 'Location: settings.php' );

?>
