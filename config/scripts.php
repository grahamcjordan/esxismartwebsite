<?php

	

?>

<script>
// Tool tips
	document.addEventListener( 'DOMContentLoaded', function() {
		var elems = document.querySelectorAll( '.tooltipped' );
		var instances = M.Tooltip.init( elems, {} );
	});
</script>

<script>
	// Select (drop down) box on Settings Page.
	// Initiates Select Box
	document.addEventListener('DOMContentLoaded', function() {
		var elems = document.querySelectorAll('select');
		var instances = M.FormSelect.init( elems,{} );
	}); 

	// Sends value to hidden input 'selectdropdown' - HD Sort
	function singleSelectChangeValue() {
		var selValue = document.getElementById( "myselect" ).value;
		document.getElementById( "selectdropdown" ).value = selValue;

	}	
</script>

<!-- Modal script -->
<script>
	$(document).ready(function(){
		$('.modal').modal({
			opacity: 0.5
		});
	})
</script>
