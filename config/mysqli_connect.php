<?php

  // variables
  $dbhost = "CHANGEME";    	// If Synology - even if using Web Station, use the IP address and not localhost
  $dbuser = "root";       		// If Synology, the user account you created on phpmyadmin. root will NOT work
  $dbpass = "CHANGEME";        // Update me
  $dbname = "ESXiDrives";       // Should be fine if default table name hasn't changed
  $dbport = "3306";             // Change to 3307 if using Synology MariaDB10 

  // connect to database
  $conn = mysqli_connect( "$dbhost", "$dbuser", "$dbpass", "$dbname", "$dbport" );
  
  // Check connection
  if ( !$conn ){ 
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
    exit();
  }
  
?>