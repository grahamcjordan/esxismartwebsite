
# README #

    Displays SMART data as a website

### Version 2.0.0

    Compatible with version 1.0.0 of ESXiSMART PowerShell module at: https://bitbucket.org/grahamcjordan but works 
    better with version 2.0.0.

    On first run you'll be taken to the settings.php page to update your config.

### PreRequisites

    Must be running the ESXiSMART PowerShell module. Available at:
    https://bitbucket.org/grahamcjordan

### How to use:
    If running XAMPP then the contents of this folder should be copied to htcdocs. It would be prudant to create a 
    new folder within htcdocs and and to here. From there, open your browser and go to http://localhost/FOLDERNAME

    If you're using Synology, make sure to install Web Station.
    If you only plan on using this as your website, you can dump the contents of this zip file directly into the shared
    folder web overwritting everything. 
    If you want to play safe, crerate a new Port based Virtual host and set the document root to web/ESXiDrives (
    copy the contents of this zip here) then access the website via http://SYNOLOGY_IP:ChosenPort

### Image licenses

    Obtained from https://pixabay.com . Free use - however because I'm not a dick, I donated!