<?php

	/*
	FYI, this displays an array in a format that's actually readable. You don't need this, but keep it for testing/further development.
	echo "<pre>";
	print_r( $ARRAYNAME );
	echo "</pre>";
	*/

    // connect to database.
    include_once "config/mysqli_connect.php";

	date_default_timezone_set('Europe/London');

	// Attempts to find table ConfigSettings (not in version 1 of website)
    $sqlConfigSettingsCreated = mysqli_query( $conn, 'select 1 from `ConfigSettings` LIMIT 1' );

	// If website is version 1, auto re-direct to settings to update
    if ( $sqlConfigSettingsCreated == FALSE ){
        header( 'Location: settings.php');
    } else {
		// pulls all data from ConfigSettings table.
        $sqlConfigSettingsGetData = 'SELECT * FROM ConfigSettings';

        // Converts mysqli query into an array.
        $sqlConfigSettings = mysqli_query( $conn, $sqlConfigSettingsGetData );
		$sqlConfigSettingsArray = [];

		while ( $row = mysqli_fetch_array( $sqlConfigSettings, MYSQLI_ASSOC ) ){
		   $sqlConfigSettingsArray[] = $row;
		}

		// Counts how many columns exist in ConfigSettings.
		$no = count( current( $sqlConfigSettingsArray ) );
		// Used to identify how many rows are on the ConfigSettings Database. In v3, this is used in header.php
		$psVersion = $sqlConfigSettingsArray[0]['PsVersionNo'];


		// If there's only three columns in ConfigSettings, database is version 2. auto-redirect to settings to update to version 3x.
		if ( $no === 3 ){
			header( 'Location: dbupgrade.php');
		} 
		
		if ( $psVersion == 3 ){ 	
			// pulls all data from ConfigSettings table.
			$esxiDataQuery = 'SELECT * FROM esxiData';

			// Converts mysqli query into an array.
			$esxiDataTable = mysqli_query( $conn, $esxiDataQuery );
			$esxiDataArray = [];
			
			while ( $row2 = mysqli_fetch_array( $esxiDataTable, MYSQLI_ASSOC ) ){
				$esxiDataArray[] = $row2;
			}
		}	
	}

	// If the config is to hide removed drives, then create a string that will be added to the $sqldrives query.
    if ( $sqlConfigSettingsArray[0]['hideremoveddrives'] == 1 ){
        $hideRemovedDrives = "WHERE IsPresent = 1";
    } else {
        $hideRemovedDrives = "";
    }

    // sql queries
    $sqldrives = "SELECT * FROM DriveInfo ORDER BY DiDriveId";

	// Gets all data from the DriveInfo table.
	$allDrivesSQL = 'SELECT * FROM DriveInfo $hideRemovedDrives;';
	$mysqliAssocDrives = mysqli_query( $conn, $allDrivesSQL );
    $allDrives = mysqli_fetch_all( $mysqliAssocDrives, MYSQLI_ASSOC );

	// Empty array
	$lastUpdatedAt = [];

	foreach ( $allDrives as $drive ){

		// driveID for each drive.
		$diDriveId = $drive['DiDriveId'];

		// Grabs most recent Date stamp, Temp recorded, and DriveID for each drive. 
		$driveTemp = "SELECT Date, Temperature, DiDriveId FROM DriveStats
			WHERE DiDriveId = $diDriveId ORDER BY Date DESC LIMIT 1;";

		$driveTempResults = mysqli_query( $conn, $driveTemp );
		$dtResults = mysqli_fetch_all( $driveTempResults, MYSQLI_ASSOC );

		// adds each records to the master array.
		array_push( $lastUpdatedAt, $dtResults );
	}

	// counts how many drives are in database, then sets $refDrive as the last inserted one.
	$howManyDrives = count( $lastUpdatedAt );
	$refDrive = $lastUpdatedAt[$howManyDrives-1];

	// Uses the last inserted drive as a reference drive for update interval time.
	$refDriveId = $refDrive[0]['DiDriveId'];

	// Gets the two latest date stamps from DriveStats database, which'll be used to calculate how often the PowerShell script has ran.
	$timeBetweenStampsQuery = "SELECT Date FROM DriveStats WHERE DiDriveId = $refDriveId ORDER BY Date DESC LIMIT 2";
	$timeBetweenStampResults = mysqli_query( $conn, $timeBetweenStampsQuery );
	$tbsResults = mysqli_fetch_all( $timeBetweenStampResults, MYSQLI_ASSOC );

	// If I knew PHP then I probably wouldn't have to convert a Date Timestamp into a Date Timestamp. Sigh.
	$date1 = new DateTime( $tbsResults[0]['Date']);
	$date2 = new DateTime( $tbsResults[1]['Date']);

	// Gets the amount of seconds between the two last updates on the last inserted drive.
	$timeDiff = $date1->getTimestamp() - $date2->getTimestamp();
	// Uses the date on the last inserted drive as a reference point to measure all time stamps against when looking for missing drives.
	$driveRefPoint = $refDrive[0]['Date'];

	// Two ints used for calculating if a drive hasn't been updated in a while.
	$xRef = [];
	$yRef = [];

	// for each item in the master array...
	foreach ( $lastUpdatedAt as $driveQuery ){

		// if the time stamp on our reference drive is older than the time stamp on (for each) drive add 1 to $x
		if ( $driveQuery[0]['Date'] > $driveRefPoint ){
			array_push( $xRef, $driveQuery );
		// if the time stamp on our reference drive is equal to time stamp on (for each) drive add 1 to $x
		} elseif ( $driveQuery[0]['Date'] === $driveRefPoint ){
			array_push( $xRef, $driveQuery );
		// if the time stamp on our reference drive is newer than the time stamp on (for each) drive...
		} else {
			// Convert datetime stamp to string
			$timeStamp = strtotime( $driveRefPoint );

			// then subtracts $timeDiff seconds from the reference drive. 
			$dateOfItem = $timeStamp - $timeDiff;
			$newTime = date( "Y-m-d H:i:s", $dateOfItem );

			// Compare the new timestamp with the (for each) drive...
			if ( $driveQuery[0]['Date'] < $newTime ){
				// if the new comparision is still out of sync (reference time stamp is newer than entry on database) then add to $y
				array_push( $yRef, $driveQuery );
			} else {
				// if the new comparision has synchorinized to an acceptable level, then add to $x
				array_push( $xRef, $driveQuery );
			}
		}
	}

	// If xRef is greater than yRef then the $lastUpdatedAt drive 0 is an acceptable reference point.
	// BadDrives are any drives that haven't synched up in $timeDiff seconds
	if ( count( $xRef ) > count( $yRef ) ){
		$badDrives = $yRef;
	} else {
		$badDrives = $xRef;
	}

	// If the HideRemoveDrives switch is enabled in the database, then for each drive in all drives where IsPresent is 0, remove from array.
	if ( $sqlConfigSettingsArray[0]['hideremoveddrives'] == 1 ){
		foreach ( $allDrives as $key => $value ){
			if ( $value['IsPresent'] == 0 ){
				unset( $allDrives[$key] );
			}
		}
	}

	// Amount of graph plot points * the amount of drives in database
    $limit = $howManyDrives * $sqlConfigSettingsArray[0]['graphplotcount'];

	// Gets * rows of stats for each drive. Used for the graph plotting, and for array_pop data
    $sqlStats = "SELECT * FROM DriveStats ORDER BY Date DESC LIMIT $limit";
    $resultStats = mysqli_query( $conn, $sqlStats );
    $allDriveStats = [];
    while ( $row = mysqli_fetch_array( $resultStats, MYSQLI_ASSOC ) ){
       $allDriveStats[] = $row;
    }

	 // Sorts drives out based on config settings hard drive order.
    if ( $sqlConfigSettingsArray[0]['hdsort'] == 1 ){
        $orderBy = "Capacity";
    } elseif ( $sqlConfigSettingsArray[0]['hdsort'] == 2 ){
        $orderBy = "Capacity";
    } elseif ( $sqlConfigSettingsArray[0]['hdsort'] == 3 ){
        $orderBy = "Drive_Controller";
    } elseif ( $sqlConfigSettingsArray[0]['hdsort'] == 4 ){
        $orderBy = "RDM_Name";
    } elseif ( $sqlConfigSettingsArray[0]['hdsort'] == 5 ){
        $orderBy = "Drive_Number";
    } else {
        $orderBy = "DiDriveId";
    }

    // Does the actual $drives sorting.
    $cols = array_column( $allDrives, $orderBy );
    array_multisort( $cols, SORT_ASC, $allDrives );

	// If sorting by size Large to small, change the sorting order.
    if ( $sqlConfigSettingsArray[0]['hdsort'] == 1 ){
        array_multisort( $cols, SORT_DESC, $allDrives );
    }

	// If sorting by drive type, first sort by SSD (spinning first), then canonical_Name in reverse (t10.ATA comes before t10.NVMe)
	if ( $sqlConfigSettingsArray[0]['hdsort'] == 6 ){
		$columns_1 = array_column( $allDrives, 'SSD' );
		$columns_2 = array_column( $allDrives, 'Canonical_Name' );
		array_multisort( $columns_1, SORT_ASC, $columns_2, SORT_ASC, $allDrives );
	}

?>


<!DOCTYPE html>
<html lang="en">

    <?php include( 'templates/header.php' ); ?>

<div>&nbsp;</div>
    <div class="container-fluid">
        <div class="row">
            <?php foreach ( $allDrives as $drive ){

				//Changes ESX CanonicalName into something far more readable
				$driveName = str_replace( 't10.ATA_____', '', htmlspecialchars( $drive['Canonical_Name'] ) );
				$driveName = str_replace( "_", " ", $driveName );
				$i = htmlspecialchars( $drive['DiDriveId'] );

				// Gets 48 hours worth of statistics of drive
				$driveStats = [];
				foreach ( $allDriveStats as $allDriveStat ) if ( $allDriveStat['DiDriveId'] == $drive['DiDriveId'] ){
					$driveStats[] = $allDriveStat;
				}

				// sorts array out by date
				$columns = array_column( $driveStats, 'Date' );
				array_multisort( $columns, SORT_ASC, $driveStats );

				// last row of array
				$statistics = array_pop( $driveStats );
									
				// initialise array
				$missingDrive = [];

				// goes through each bad drive and sets missingDrive array to that of any matching the DriveId
				foreach ( $badDrives as $badItem ){
					if ( $i == $badItem[0]['DiDriveId'] ){
						$missingDrive = $badItem[0];
					}
				}

				// Set temp and updated at based on if drive is present, and if drive has breached its thresholds.
				if ( count( $missingDrive ) > 1 ){
					$driveTempCurrent = "<strong>DRIVE IS OFFLINE</strong>";
					$lastUpdatedAt    = "<strong>" . htmlspecialchars( date( "d M Y H:i A", strtotime( $missingDrive['Date'] ) ) ) . "</strong>";
					$driveStatusImg   = "images/cross.svg";
					$driveTooltipped  = "Drive is offline or has been removed";
				} elseif ( $statistics['Temperature'] < $drive['DriveTempLow'] || $statistics['Temperature'] > $drive['DriveTempHigh'] ) {
					$driveTempCurrent = "<strong>" . htmlspecialchars( $statistics['Temperature'] ) .  "&deg;C THRESHOLD BREACHED</strong>";
					$lastUpdatedAt    = htmlspecialchars( date( "d M Y H:i A", strtotime( $statistics['Date'] ) ) );
					$driveStatusImg   = "images/cross.svg";
					$driveTooltipped  = "Drive Temperature has breached its threshold";
				} else {
					$driveTempCurrent = "Current Temperature: " . htmlspecialchars( $statistics['Temperature'] ) . " &deg;C";
					$lastUpdatedAt    = htmlspecialchars( date( "d M Y H:i A", strtotime( $statistics['Date'] ) ) );
					$driveStatusImg   = "images/ok.svg";
					$driveTooltipped  = "Drive is in good condition";
				}

				if ( !$statistics['Health'] == 'OK' ){
					$driveStatusImg   = "images/cross.svg";
					$driveTooltipped  = "Errors found on drive";
				}
				if ( $driveTempCurrent === "<strong>DRIVE IS OFFLINE</strong>" ){
					$driveTooltipped = "Drive is offline or has been removed";
				}

				// HD Images. If database marked as SSD...
				if ( $drive['SSD'] == 1 ){
					// ... and CanonicalName matches NVMe then set the image to m2 image.
					if ( strpos( $drive['Canonical_Name'], "NVMe" )){ 
						$driveName  = str_replace( 't10.NVMe', '', $drive['Canonical_Name'] );
						$driveName  = str_replace( "_", " ", $driveName ); 
						$hdImage    = "images/nvme.svg";
						$hdClass    = "imagesm2";
						$cardColour = "blue lighten-2";
					// if drive does not match NVMe then set image to standard SSD image.
					} else { 
						$hdImage    = "images/ssd.png";
						$hdClass    = "images";
						$cardColour = "blue lighten-3";
					}
				} else {
					// Set drive Image to spinning disk.
					$hdImage 	= "images/hdd.svg";
					$hdClass 	= "images";
					$cardColour = "blue lighten-4";
				}

			?>

                <!-- for mobile phones, this should be <div class="col s12 md3">  Anything less than 720px -->
                <div class="col s12 m4 l3">
                    <div class="card z-depth-2 <?php echo $cardColour; ?>">

                        <!-- HDD Images -->
                        <img src="<?php echo $hdImage; ?>" class="<?php echo $hdClass; ?>">

                        <!-- Reveal card  upper limit start -->
                        <div class="card">
                            <div class="card-image waves-effect waves-block waves-light">

                                <!-- Drive stats -->
                                <div class="card-content center">
                                    <h6><?php echo $driveName;?></h6>
                                    <ul>
										<!-- Drive Temp threshold breached checks -->
                                        <li><?php echo "Drive Size: " . htmlspecialchars( $drive['Capacity'] ) . " GB" ?></li>
                                        <li><?php echo $driveTempCurrent; ;?></li>
                                        <li>&nbsp;</li>
                                        <li>Last updated at <?php echo $lastUpdatedAt;?></li>
                                    </ul>
                                </div>

								<img src=<?php echo $driveStatusImg; ?> class="imagestatus tooltipped" data-position="bottom-right" data-delay="50" data-tooltip="<?php echo $driveTooltipped; ?>">

								<br /> <br /><br />
                            </div>

                            <!-- reveal card "click me" link -->
                            <div class="card-content" >
                                <span class="card-title activator grey-text text-darken-4 reveal"><i class="right">Click for more info</i></span>
                            </div>

                            <!-- reveal card content" -->
                            <div class="card-reveal">
                               <span class="card-title grey-text text-darken-4 reveal"><i class="right">close</i></span>
                               <div class="extradata">
                                   <br />VM Host: <i><?php echo htmlspecialchars( $drive['VM_Host'] );?></i>
                                   <br />Drive Host Virtual Machine: <i><?php echo htmlspecialchars( $drive['Drive_Host'] );?></i>
                                   <br />Hard Disk Controller Location: <i><?php echo htmlspecialchars( $drive['Drive_Controller'] );?></i>
                                   <br />Hard Disk No.: <i><?php echo htmlspecialchars( $drive['Drive_Number'] );?></i>
                                   <br />Raw Device Mapping Name: <i><?php echo htmlspecialchars( $drive['RDM_Name'] );?></i>
                                   <br />
                                   <br />Read Error: <i><?php echo htmlspecialchars( $statistics['ReadError'] ); ?></i>
                                   <br />Read Error Threshold: <i><?php echo htmlspecialchars( $statistics['ReadErrorThreshold'] ); ?></i>
                                   <br />Read Error Worst: <i><?php echo htmlspecialchars( $statistics['ReadErrorWorst'] ); ?></i>
                                   <br />
                                   <br />Write Error: <i><?php echo htmlspecialchars( $statistics['WriteError'] ); ?></i>
                                   <br />Write Error Threshold: <i><?php echo htmlspecialchars( $statistics['WriteErrorThreshold'] ); ?></i>
                                   <br />Write Error Worst: <i><?php echo htmlspecialchars( $statistics['WriteErrorWorst'] ); ?></i>
                                   <br />
                                   <br />Power Cycle Count: <i><?php echo htmlspecialchars( $statistics['PowerCycleCount'] ); ?></i>
                                   <br />Re-allocated Sector Count: <i><?php echo htmlspecialchars( $statistics['ReallocatedSectorCount'] ); ?></i>
                                   <br />Raw Read Error Rate: <i><?php echo htmlspecialchars( $statistics['RawReadErrorRate'] ); ?></i>
                                </div>
                            </div>
                        </div>
                    </div>
					
					<!-- Modal for each drive. Sits outside of card but offset so appears inside -->

					<div class="container center">
						<a class="waves-effect waves-light btn blue darken-1 modal-trigger modalPosition" href="#modal<?php echo $i;?>">Launch Temperature History</a>

						<div id="modal<?php echo $i;?>" class="modal">
							<div class="modal-content">

								<!-- Creates Temp chart within a card so as to give border affect -->
								<div class="card graphcolour">
									<div class="card graphcontent">
										<h6>Drive Temperature fluctuations over <?php echo $sqlConfigSettingsArray[0]['graphplotcount'] ; ?> records</h6>
										<h5><?php echo $driveName; ?></h5>

										<canvas id=<?php echo htmlspecialchars( $drive['DiDriveId'] )?>></canvas>
									</div>
								</div>

								<script type="text/javascript">
									var chr2=document.getElementById( <?php echo htmlspecialchars( $drive['DiDriveId'] )?> ).getContext("2d");

									var myChart2=new Chart( chr2,{
										type:'line',
										data:{
											labels:[
												<?php
													// loop though each graph plot count and add the date temperature recorded to the x data point.
													for ( $x = $sqlConfigSettingsArray[0]['graphplotcount'] +1; $x > 2; $x-- ){
														echo "'" . htmlspecialchars( date( "d M H:i", strtotime( array_slice( $driveStats, -$x )[0]['Date'] ) ) ) . "',"; 
													}

													// the last date point is outside of the loop and contains no trailing comma.
													echo "'" . htmlspecialchars( date( "d M H:i", strtotime( $statistics['Date'] ) ) ) . "'";

												?>
											],
											datasets:[{
												label:'Temperature',
												data:[
													<?php
														// loop through each graph plot count and add the temperature onto each rollover plot point.
														for ( $x = $sqlConfigSettingsArray[0]['graphplotcount'] +1; $x > 2; $x-- ){
															echo "'" . htmlspecialchars( array_slice( $driveStats, -$x )[0]['Temperature'] ) . "',"; 
														}

														// the last temperature point is outside of the loop and contains no trailing comma.
														echo "'" . htmlspecialchars( $statistics['Temperature'] ) . "'";
													?>
												],
												backgroundColor:'rgba(0,0,0,0)',
												borderColor:'#000',
												borderWidth:0,
											}]
										},
										options:{
											legend:{
												labels:{
													fontColor:'#000',
												}
											}
										}
									});
								</script>
							</div>
						</div>
					</div>
					<div>&nbsp;</div>
                </div>	
            <?php } ?>	
        </div>
    </div>

	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

    <?php include( 'templates/footer.php'); ?>

</html>
