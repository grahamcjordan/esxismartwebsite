
### Version 3.0.0

   - BugFixes
      When PowerShell script is ran twice within half an hour, the settings page borks. 
      There was a note on here to fix this before publishing version 2.0. Obviously didn't pay attention.
      $sqldrives = 'SELECT driveinfo.* 
	   FROM driveinfo INNER JOIN drivestats ON drivestats.DiDriveId = driveinfo.DiDriveId 
        WHERE drivestats.Date > NOW() -INTERVAL "30" DAY_MINUTE';

   - Features
      New drive threshold for shutting down NAS.
      Temperature charts can now be 
      NAS hardware monitor. CPU and memory.
      Datastore checks. How much space is free. 
      Now accounts for NVMe drives.



### Version 2.0.0

 - Added settings page.
    Allows for alternative ordering of hard drives.
    Allows for a variable amount of plot points on temperature graph.
    Individual drive temperature thresholds configurable.
    Hide removed drives.
 - Index.php
    Bug fixes. 
    Code improvement.

### Version 1.0.0

 - Release


