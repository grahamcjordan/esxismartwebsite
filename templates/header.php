
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ESX SMART Data</title>
    <!-- Compiled and minified CSS -->
    <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.css">
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link type="text/css" rel="stylesheet" href="extras/nouislider.css">

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="extras/nouislider.min.js"></script>

    <style type="text/css">
        #switch-panel {
            background-color: yellow;
        }
        .brand{
            background: #cbb09c !important;
        }
        .brand-text{
            color: black !important;
        }
        .settings{
            background: green !important;
        }
        .settingsmain{
            padding-left:320px;
        }
        .settingsform{
            color: black !important;
        }
        form{
            max-width: 600px;
            margin: 20px auto;
            padding: 20px;
        }
        .images{
            width: 100px;
            height: 120px;
            margin: 40px auto -30px;
            display: block;
            position: relative;
            top: -30px;
        }
		.imagesm2{
            width: 100px;
            height: 130px;
            margin: 40px auto -40px;
            display: block;
            position: relative;
            top: -50px;
        }
        .imageESXi{
            width: 64px;
            height: 64px;
            display: block;
            position: relative;
			font-size: 7px !important;
        }
        .imagestatus{
            width: 80px;
            height: 80px;
            margin: auto;
            display: block;
            position: relative;  
        }
        .imagesettings{
            width: 60px;
            height: 60px;
            margin: -15px auto;
            display: block;
            position: relative;
        }
        .reveal{
            font-size: 14px !important;
        }
        .extradata{
            font-size: 14px !important;
            padding: 60px;
        }
        .graphcontent{
            padding: 10px;
            height:80%;
        }
        .graphcolour{
            background: rgba(255,255,255,255);
        }
		.modalPosition{
			top: -100px;
		}
		.ESXUsage{
			width: 100px;
			font-size: 9px !important;
		}		

    </style>
</head>
<body class="green lighten-1">

	<?php include( 'config/scripts.php' ); ?>

    <nav class="white z-depth-3">
        <div class="container">

			<?php

				// If PsVersion is 3 or higher, get ESXiData and work out how the percentage of use for CPU, Memory, and Datastore
				if ( $psVersion == 3 ){ 

					$memMaths = $esxiDataArray[0]['MemUsage'] / $esxiDataArray[0]['MemTotal'];
					$memUsagePcnt = $memMaths * 100;
					$memFree = $esxiDataArray[0]['MemTotal'] - $esxiDataArray[0]['MemUsage'];

					$cpuMaths = $esxiDataArray[0]['CPUUsage'] / $esxiDataArray[0]['CPUTotal'];
					$cpuUsagePcnt = $cpuMaths * 100;

					$dsMaths = $esxiDataArray[0]['DataStoreUsage'] / $esxiDataArray[0]['DataStoreTotal'];
					$datastoreUsagePcnt = $dsMaths * 100;
					$dsFree = $esxiDataArray[0]['DataStoreTotal'] - $esxiDataArray[0]['DataStoreUsage'];

				?>

				<!-- Adds an ESXi logo to thetop left, then creates a tooltip withCPU, Memory and Datastore utilisation info -->
				<ul id="nav-mobile" class="left hide-on-small-and-down">
					<span class="black-text left">
						<img 
							src           = images/esxi.png 
							class         = "imageESXi tooltipped" 
							data-position = "left" 
							data-tooltip  = "CPU    : <?php echo number_format( $cpuUsagePcnt, 2 ); ?>% utilisation.<br />
											 Memory : <?php echo number_format( $memUsagePcnt, 2 ); ?>% utilisation. (<?php echo $memFree; ?> GB free) <br />
											 DATAStore :  <?php echo number_format( $datastoreUsagePcnt, 2 ); ?>% utilisation. (<?php echo $dsFree; ?> GB free)">
					</span>
					
					<div class="progress ESXUsage grey lighten-4">
						<div class="determinate green" style="width: <?php echo number_format( $cpuUsagePcnt, 2 );?>%";></div>
					</div>
					<div class="progress ESXUsage grey lighten-4">
						<div class="determinate blue" style="width: <?php echo number_format( $memUsagePcnt, 2 );?>%";></div>
					</div>
					<div class="progress ESXUsage grey lighten-4">
						<div class="determinate light-blue lighten-4" style="width: <?php echo number_format( $datastoreUsagePcnt, 2); ?>%";></div>
					</div>
				</ul>
				
				
			<?php }?>
            <a href="index.php" class="center brand-logo brand-text">SMART Data</a>
            <ul id="nav-mobile" class="right hide-on-small-and-down">
                <a href="settings.php" class="btn settings z-depth-2">Settings</a>
            <ul>
        </div>
    </nav>